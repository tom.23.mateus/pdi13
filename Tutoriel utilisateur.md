#AgridataValue_Calculator
***
Python script for calcul of vulnerability and agro-climatic indicator

## Table of Contents
1. [Installation](#installation)
2. [Utilisation](#Utilisation)
3. [Technologies](#technologies)

## Installation
***
No installation needed 

Installation of IDE Needed

Only execute python file name "main"

## Utilisation
***
* execute main
* input need for rcp choice : anwser possible : "4.5"/"1" for rcp4.5 and "8.5"/"2" for rcp8.5
* input need for visualisation choices : anwser possible : "yes"/"y" or "no"/"n" for plot or not matplolib map of agro-climatic indicator

Output in File "sorti_shp/..."
Save of vulnerability map visualisation in "python" file


## Technologies
***
The project use Python
A list of Python librairies used within the project:
* [Numpy](https://numpy.org/): Version 1.26.4
* [Panda](https://pandas.pydata.org/): Version 2.2.1
* [Geopandas](https://geopandas.org/en/stable/): Version 0.8.1
* [Matplotlib](https://matplotlib.org/): Version 3.8.0
