#AgridataValue_Calculator
***
Python script for calcul of vulnerability and agro-climatic indicator


## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)

## General Info
***
Calcul of agro-climatic indicators:
* nombre_jour_chaud / number of hot day (T°C > 30°C)
* nombre_jour_gel / number of freezing day (T°C < 0°C)
* nombre_de_vague_de_froid / number of freeze wave (Accodring to the meteo France definition [https://meteofrance.com/comprendre-la-meteo/temperatures/quest-ce-quune-vague-de-froid])
* maximum_precipitation_solide_journalière / maximum of solid precipitation 
* total_precipitation_solide / total of solid precipitation

Years of study :2030 ,2050 ,2070 ,2100

Rcp : 4.5 , 8.5 
Definition of rcp : https://www.drias-climat.fr/accompagnement/sections/175


### Input
***
File "donnes/..."
* "temps_2030_4.5", "temps_2050_4.5","temps_2070_4.5","temps_2100_4.5","temps_2030_8.5","temps_2050_8.5","temps_2070_8.5","temps_2100_8.5" : Shapefiles , name of columnes : 0,1,2,3,4,5,6,7 , Data : Y , X , month, day, year , minimal temperature, mean temperature,solid precipitation
Each files concern one years and on rcp specify in the name of the files
[Month , day ,year] Unused 
* "SaintEmilion.shp" : Shapefiles , limite of the study area
* "Sol_1","Sol_2","Sol_3","Sol_4","Sol_5": Shapefiles , limite of the geological area for exposition and vulnerability calcul 
[Sol_4]Unused 

### Output
***
File "sorti_shp/..."
1 one file  for each year and rcp for vulnerability and for each agro-climatic indicators

### Variable
Possibility to change variable :

*  "r" to change the resolution of the interpolation (r =100 give a ~ 200m resolution) (in main file)
*  "p" to change the parametre of the interpolation by inverse distance (in visualisation file)
*  Coefficient in the file classification to change the pondaration of indicators


## Technologies
***
The project use Python
A list of Python librairies used within the project:
* [Numpy](https://numpy.org/): Version 1.26.4
* [Panda](https://pandas.pydata.org/): Version 2.2.1
* [Geopandas](https://geopandas.org/en/stable/): Version 0.8.1
* [Matplotlib](https://matplotlib.org/): Version 3.8.0

## Installation
***
No installation needed 

IDE Needed

Only execute python file name "main"

