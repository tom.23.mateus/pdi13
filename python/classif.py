# -*- coding: utf-8 -*-
"""
Created on Fri May 10 13:45:46 2024

@author: Tom
"""
import pandas as pd
import geopandas as gpd
import numpy as np
import visualisation as vis


def classification(nb_j_chaud,nb_j_gel,precipitation_s_tot,rcp,annee):
    """
    Calcule la vulnérabilité et effectue une classification

    Args:
        nb_j_chaud (str): nom du fichier contenant l'indicateur de chaleur type "nombre_jour_chaud_2030_rcp".
        nb_j_gel (str): nom du fichier contenant l'indicateur de froids type "nombre_jour_froid_2030_rcp".
        precipitation_s_tot(str): nom du fichier contenant l'indicateur de precipitation solide type "total_precipitation_solide_2100_rcp".
        rcp(flaot):
        annee(float):
        
    """
    
    # Charger le fichier de points et de limite
    c_gdf = gpd.read_file('sortie_shp/'+nb_j_chaud+'.shp')
    f_gdf = gpd.read_file('sortie_shp/'+nb_j_gel+'.shp')
    p_gdf = gpd.read_file('sortie_shp/'+precipitation_s_tot+'.shp')
    
    
    sol1_gdf = gpd.read_file('donnees/sol_1.shp')
    sol2_gdf = gpd.read_file('donnees/sol_2.shp')
    sol3_gdf = gpd.read_file('donnees/sol_3.shp')
    # sol4_gdf = gpd.read_file('donnees/sol_4.shp')
    sol5_gdf = gpd.read_file('donnees/sol_5.shp')
    
    c_gdf = c_gdf.to_crs('EPSG:4326')
    f_gdf = f_gdf.to_crs('EPSG:4326')
    p_gdf = p_gdf.to_crs('EPSG:4326')
    
    # mc,Mc = min(f_gdf['nombre_jou']) ,max(f_gdf['nombre_jou'])
    # vis.plot_patch(f_gdf['X'], f_gdf['Y'], f_gdf['nombre_jou'], xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "Classification en fonction de la vulnérabilité " , cmap = 'inferno',minmax = [ mc, Mc])
    # vis.plot_patch(X, Y, Data, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "Classification en fonction de la vulnérabilité " , cmap = 'inferno',minmax = [ 0, 4])
    # vis.plot_patch(X, Y, Data, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "Classification en fonction de la vulnérabilité " , cmap = 'inferno',minmax = [ 0, 4])
    
    
    
    
    
    sol1_gdf = sol1_gdf.to_crs('EPSG:4326')
    sol2_gdf = sol2_gdf.to_crs('EPSG:4326')
    sol3_gdf = sol3_gdf.to_crs('EPSG:4326')
    # sol4_gdf = sol4_gdf.to_crs('EPSG:4326')
    sol5_gdf = sol5_gdf.to_crs('EPSG:4326')
    
    
    # Unifier la limite en un seul polygone
    sol1_unifiee = sol1_gdf.unary_union
    sol2_unifiee = sol2_gdf.unary_union
    sol3_unifiee = sol3_gdf.unary_union
    # sol4_unifiee = sol4_gdf.unary_union
    sol5_unifiee = sol5_gdf.unary_union
    
    # Effectuer une opération d'intersection entre les points et la limite
    c1s_gdf = c_gdf[c_gdf.geometry.within(sol1_unifiee)]
    c2s_gdf = c_gdf[c_gdf.geometry.within(sol2_unifiee)]
    c3s_gdf = c_gdf[c_gdf.geometry.within(sol3_unifiee)]
    # c4s_gdf = c_gdf[c_gdf.geometry.within(sol4_unifiee)]
    c5s_gdf = c_gdf[c_gdf.geometry.within(sol5_unifiee)]
    
    # Effectuer une opération d'intersection entre les points et la limite
    f1s_gdf = f_gdf[f_gdf.geometry.within(sol1_unifiee)]
    f2s_gdf = f_gdf[f_gdf.geometry.within(sol2_unifiee)]
    f3s_gdf = f_gdf[f_gdf.geometry.within(sol3_unifiee)]
    # f4s_gdf = f_gdf[f_gdf.geometry.within(sol4_unifiee)]
    f5s_gdf = f_gdf[f_gdf.geometry.within(sol5_unifiee)]
    
    # Effectuer une opération d'intersection entre les points et la limite
    p1s_gdf = p_gdf[p_gdf.geometry.within(sol1_unifiee)]
    p2s_gdf = p_gdf[p_gdf.geometry.within(sol2_unifiee)]
    p3s_gdf = p_gdf[p_gdf.geometry.within(sol3_unifiee)]
    # p4s_gdf = p_gdf[f_gdf.geometry.within(sol4_unifiee)]
    p5s_gdf = p_gdf[p_gdf.geometry.within(sol5_unifiee)]
    
    #renommer les index
    t = []
    for i in range(np.shape(c1s_gdf)[0]):
        t.append(i)
        
    c1s_gdf = c1s_gdf.set_axis(t,axis=0)
    f1s_gdf = f1s_gdf.set_axis(t,axis=0)
    p1s_gdf = p1s_gdf.set_axis(t,axis=0)
    
    t = []
    for i in range(np.shape(c2s_gdf)[0]):
        t.append(i)
    
    c2s_gdf = c2s_gdf.set_axis(t,axis=0)
    f2s_gdf = f2s_gdf.set_axis(t,axis=0)
    p2s_gdf = p2s_gdf.set_axis(t,axis=0)
    
    t = []
    for i in range(np.shape(c3s_gdf)[0]):
        t.append(i)
    
    c3s_gdf = c3s_gdf.set_axis(t,axis=0)
    f3s_gdf = f3s_gdf.set_axis(t,axis=0)
    p3s_gdf = p3s_gdf.set_axis(t,axis=0)
    
    # t = []
    # for i in range(np.shape(c4s_gdf)[0]):
    #     t.append(i)
    
    # c4s_gdf = c4s_gdf.set_axis(t,axis=0)
    # f4s_gdf = f4s_gdf.set_axis(t,axis=0)
    # p4s_gdf = p4s_gdf.set_axis(t,axis=0)
    
    t = []
    for i in range(np.shape(c5s_gdf)[0]):
        t.append(i)
    
    c5s_gdf = c5s_gdf.set_axis(t,axis=0)
    f5s_gdf = f5s_gdf.set_axis(t,axis=0)
    p5s_gdf = p5s_gdf.set_axis(t,axis=0)
    
    #ponderation
    c1s_gdf['nombre_jou'] = c1s_gdf['nombre_jou'] * 1
    f1s_gdf['nombre_jou'] = f1s_gdf['nombre_jou'] * 1.08
    p1s_gdf['total_prec'] = p1s_gdf['total_prec'] *2
    
    c2s_gdf['nombre_jou'] = c2s_gdf['nombre_jou'] * 1.02
    f2s_gdf['nombre_jou'] = f2s_gdf['nombre_jou'] * 1.06
    p2s_gdf['total_prec'] = p2s_gdf['total_prec'] *2
    
    c3s_gdf['nombre_jou'] = c3s_gdf['nombre_jou'] * 1.04
    f3s_gdf['nombre_jou'] = f3s_gdf['nombre_jou'] * 1.04
    p3s_gdf['total_prec'] = p3s_gdf['total_prec'] *2
    
    # c4s_gdf['nombre_jou'] = c4s_gdf['nombre_jou'] * 1.06
    # f4s_gdf['nombre_jou'] = f4s_gdf['nombre_jou'] * 1.02
    # p4s_gdf['total_prec'] = p4s_gdf['total_prec'] *2
    
    c5s_gdf['nombre_jou'] = c5s_gdf['nombre_jou'] * 1.08
    f5s_gdf['nombre_jou'] = f5s_gdf['nombre_jou'] * 1
    p5s_gdf['total_prec'] = p5s_gdf['total_prec'] *2
    
    X = []
    Y = []
    Data = []
    
    #Calcul de vulnérabilité
    for i in range(np.shape(c1s_gdf)[0]):
        X.append(c1s_gdf['X'][i])
        Y.append(c1s_gdf['Y'][i])
        Data.append(c1s_gdf['nombre_jou'][i]+f1s_gdf['nombre_jou'][i]+p1s_gdf['total_prec'][i])
        
    for i in range(np.shape(c2s_gdf)[0]):
        X.append(c2s_gdf['X'][i])
        Y.append(c2s_gdf['Y'][i])
        Data.append(c2s_gdf['nombre_jou'][i]+f2s_gdf['nombre_jou'][i]+p2s_gdf['total_prec'][i])
        
    for i in range(np.shape(c3s_gdf)[0]):
        X.append(c3s_gdf['X'][i])
        Y.append(c3s_gdf['Y'][i])
        Data.append(c3s_gdf['nombre_jou'][i]+f3s_gdf['nombre_jou'][i]+p3s_gdf['total_prec'][i])
        
    # for i in range(np.shape(c4s_gdf)[0]):
    #     X.append(c4s_gdf['X'][i])
    #     Y.append(c4s_gdf['Y'][i])
    #     Data.append(c4s_gdf['nombre_jou'][i]+f4s_gdf['nombre_jou'][i]+p4s_gdf['total_prec'][i])
        
    for i in range(np.shape(c5s_gdf)[0]):
        X.append(c5s_gdf['X'][i])
        Y.append(c5s_gdf['Y'][i])
        Data.append(c5s_gdf['nombre_jou'][i]+f5s_gdf['nombre_jou'][i]+p5s_gdf['total_prec'][i])
        
    m = min(Data)
    M = max(Data)
    p = (M-m)/5
    
    #Classification
    for i in range (len(Data)):
        if Data[i]-m > p*4:
            Data[i] = 4
        else:
            if Data[i]-m > p*3:
                Data[i] = 3
            else:
                if Data[i] -m > p*2:
                    Data[i] = 2
                else :
                    if Data[i]-m > p :
                        Data[i] = 1
                    else:
                        Data[i] = 0
                
                
            
        
    df = pd.DataFrame({'X': X, 'Y': Y, 'classif':Data})
    gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.X, df.Y), crs="EPSG:4326")
    gdf.to_file('sortie_shp/Vulnérabilité_'+str(annee)+'_rcp'+str(rcp)+'.shp')
    vis.plot_patch(X, Y, Data, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "Classification en fonction de la vulnérabilité "+str(annee)+'_rcp'+str(rcp),fileo ='Vulnérabilité_'+str(annee)+'_rcp'+str(rcp)+'.png' , cmap = 'summer',minmax = [ 0,4])
    


print('calcul de la Vulnérabilité')

rcp = 4.5
classification('nombre_jour_chaud_2030_rcp'+str(rcp), 'nombre_jour_gel_2030_rcp'+str(rcp), 'total_precipitation_solide_2030_rcp'+str(rcp), rcp, '2030')
classification('nombre_jour_chaud_2050_rcp'+str(rcp), 'nombre_jour_gel_2050_rcp'+str(rcp), 'total_precipitation_solide_2050_rcp'+str(rcp), rcp, '2050')
classification('nombre_jour_chaud_2070_rcp'+str(rcp), 'nombre_jour_gel_2070_rcp'+str(rcp), 'total_precipitation_solide_2070_rcp'+str(rcp), rcp, '2070')
classification('nombre_jour_chaud_2100_rcp'+str(rcp), 'nombre_jour_gel_2100_rcp'+str(rcp), 'total_precipitation_solide_2100_rcp'+str(rcp), rcp, '2100')

rcp = 8.5
classification('nombre_jour_chaud_2030_rcp'+str(rcp), 'nombre_jour_gel_2030_rcp'+str(rcp), 'total_precipitation_solide_2030_rcp'+str(rcp), rcp, '2030')
classification('nombre_jour_chaud_2050_rcp'+str(rcp), 'nombre_jour_gel_2050_rcp'+str(rcp), 'total_precipitation_solide_2050_rcp'+str(rcp), rcp, '2050')
classification('nombre_jour_chaud_2070_rcp'+str(rcp), 'nombre_jour_gel_2070_rcp'+str(rcp), 'total_precipitation_solide_2070_rcp'+str(rcp), rcp, '2070')
classification('nombre_jour_chaud_2100_rcp'+str(rcp), 'nombre_jour_gel_2100_rcp'+str(rcp), 'total_precipitation_solide_2100_rcp'+str(rcp), rcp, '2100')

        
            
    
    
    
