# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 09:09:58 2024

@author: Tom
"""
# Numpy
import numpy as np
# Matplotlib / plot
import matplotlib.pyplot as plt
    
from matplotlib import cm

#####################kriegeage###################


def interp_inv_dist(x_grd,y_grd,x_obs,y_obs,z_obs,p):
     """
     calcul de l'interpolation par inverse de distance
     """
     z = np.nan*np.zeros(x_grd.shape)
     d=[]

     for i in np.arange(0,x_grd.shape[0]):
         for j in np.arange(0,x_grd.shape[1]):
             
              d = np.sqrt((x_grd[i,j]-x_obs)**2+(y_grd[i,j]-y_obs)**2)
              n =np.sum(z_obs/d**p)
              dn = np.sum(1/d**p)
              z[i,j] = n/dn 
     return z

def dist(x1,x2,y1,y2):
    """
    calcul de distance
    """
    return np.sqrt((x2-x1)**2+(y2-y1)**2)

def semi_variogramme(x_obs,y_obs,z_obs):
    """
    calcul du semi variogramme
    """
    h=[]
    delta_z =[]
    for i in range(len(x_obs)):
        for j in range (len(x_obs)-i):
            h.append(dist(x_obs[i],x_obs[i+j],y_obs[i],y_obs[i+j]))
            delta_z.append(((z_obs[i]-z_obs[i+j])**2)/2)
        
    return np.array(delta_z) , np.array(h)

def calc_nuee(x_obs, y_obs, z_obs):
    """
    calcul de la nuée
    """
    
    ecart = []
    distance = []
    for i in range(len(x_obs)):
        for j in range(len(x_obs)):
            Delta  =  ( 1/2 * (z_obs[i] - z_obs[j] ) **2)
            ecart.append(Delta)
            dist = np.sqrt((x_obs[i]-x_obs[j])**2+(y_obs[i] -y_obs[j])**2)
            distance.append(dist)
            
    return  np.array(distance), np.array(ecart)


def calc_var_exp(x_obs, y_obs, z_obs, hmax, nmax):
    """
    calcul de la variation expérimentale
    """
        
    distance,ecart = calc_nuee(x_obs, y_obs, z_obs)
    moy_e = []
    moy_d = []
    # i = 0
    
    idx = np.argsort(distance)
    # distance triée : distance[idx]
    
    
    for k in range(0,len(idx),nmax):
        idxk = idx[k:k+nmax]
        
        moyenne_ecart = np.mean(ecart[idxk])
        moyenne_distance = np.mean(distance[idxk])
        
        moy_e.append(moyenne_ecart )
        moy_d.append(moyenne_distance)
 
    return moy_d, moy_e




def d_a(h,a,c):
    if h <= a :
        return (-14*h**2/a**3 + 105*h**3/(4*a**4) - 35*h**5/(2*a**6) + 21*h**7/(4*a**8))*c
    else :
        return 0

def d_c(h,a,c):
    if h <= a :
       return 7*h**2/a**2 - 35*h**3/4/a**3 + 7*h**5/2/a**5 - 3*h**7/4/a**7
    else :
       return 1
def gamma(h,a,c):
    if h <= a :
        return c*(7*h**2/a**2 - 35*h**3/4/a**3 + 7*h**5/2/a**5 - 3*h**7/4/a**7)
    else :
        return c
    

def mat_a (h,a0,c0):
    mat = []
    for i in  range (len(h)):
        
        mat.append([d_a(h[i],a0,c0),d_c(h[i],a0,c0)])
    return mat

def mat_b (g_exp,h_exp,a0,c0) :
    mat= []
    for i in range (len(h_exp)):
        mat.append([g_exp[i]-gamma(h_exp[i],a0,c0)])
    return mat



#####################plot##########################



def plot_points(x_obs, y_obs, xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé des sites d'observations
    # x_obs, y_obs : observations
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    
    fig = plt.figure()
    ax = plt.gca()
    
    plt.plot(x_obs, y_obs, 'ok', ms = 4)
    dx = max(x_obs)-min(x_obs)
    dy = max(y_obs)-min(y_obs)
    minx = min(x_obs)-0.05*dx; maxx = max(x_obs)+0.05*dx
    miny = min(y_obs)-0.05*dy; maxy = max(y_obs)+0.05*dy
    #ax.set_aspect('equal', adjustable='box')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')
    return plt.gca()

def plot_patch(x_obs, y_obs, z_obs, xlabel = "", ylabel = "", zlabel = "", title = "", fileo = "", cmap = "", minmax =""):
    # Tracé des valeurs observées
    # x_obs, y_obs, z_obs : observations
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    # cmap : nom de la carte de couleur
    
    fig = plt.figure()
    p=plt.scatter(x_obs, y_obs, marker = 'o', c = z_obs, s = 80, cmap=cmap, vmin = minmax[0], vmax = minmax[-1])

    dx = max(x_obs)-min(x_obs)
    dy = max(y_obs)-min(y_obs)
    minx = min(x_obs)-0.05*dx; maxx = max(x_obs)+0.05*dx
    miny = min(y_obs)-0.05*dy; maxy = max(y_obs)+0.05*dy
    plt.xlim([minx,maxx])
    plt.ylim([miny,maxy])
    #plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')
    return plt.gca()

def plot_surface_2d(x_grd ,y_grd ,z_grd, x_obs = np.array([]) ,y_obs = np.array([]), minmax = [0,0], xlabel = "", ylabel = "", zlabel = "", title = "", fileo = "", cmap = cm.terrain):
    # Tracé du champ interpolé sous forme d'une surface colorée
    # x_grd, y_grd, z_grd : grille de valeurs interpolées
    # x_obs, y_obs : observations (facultatif)
    # minmax : valeurs min et max de la variable interpolée (facultatif)
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    # cmap : nom de la carte de couleur
    z_grd_m = np.ma.masked_invalid(z_grd)
    fig = plt.figure()
    if minmax[0] < minmax[-1]:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cmap, vmin = minmax[0], vmax = minmax[-1], shading = 'auto')
    else:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cmap, shading = 'auto')
    if len(x_obs)>0:
        plt.scatter(x_obs, y_obs, marker = 'o', c = 'k', s = 5)
        dx = max(x_obs)-min(x_obs)
        dy = max(y_obs)-min(y_obs)
        minx = min(x_obs)-0.05*dx; maxx = max(x_obs)+0.05*dx
        miny = min(y_obs)-0.05*dy; maxy = max(y_obs)+0.05*dy
    else:
        dx = np.max(x_grd)-np.min(x_grd)
        dy = np.max(y_grd)-np.min(y_grd)
        minx = np.min(x_grd)-0.05*dx; maxx = np.max(x_grd)+0.05*dx
        miny = np.min(y_grd)-0.05*dy; maxy = np.max(y_grd)+0.05*dy
    plt.xlim([minx,maxx])
    plt.ylim([miny,maxy])
    # plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')
    return plt.gca()
    
    

