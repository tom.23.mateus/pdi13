# -*- coding: utf-8 -*-
"""
Created on Tue May  7 13:48:19 2024

@author: Tom
"""

def nb_j_gel(x,y,tm):
    """
    Calculer le nombre de jour de gel annuel pour chauqe point.

    Args:
        x (list: Coordonnées X des previsions aladin.
        y (list): Coordonnées Y des previsions aladin.
        tm (list): Temperature minimal journalière des previsions aladin.
        

    Returns:
        X(list): Coordonnées X .
        Y(list): Coordonnées Y .
        nb_j(list): Nombre de jour de gel annuel pour chaque point
    """
    X = []
    Y = []
    nb_j = []
    c = 0
    i =0
    j=0

    while i < len(tm):
       X.append(x[i])
       Y.append(y[i])
       while x[i] == X[j] and y[i] == Y[j] : 
           if tm[i] < 0 :
               c+=1
           i+=1
           if i >= len(tm):
               break
       nb_j.append(c)
       c=0
       j+=1
    return X,Y,nb_j

def nb_j_chaud(x,y,t):
    """
    Calculer le nombre de jour de gel annuel pour chauqe point.

    Args:
        x (list: Coordonnées X des previsions aladin.
        y (list): Coordonnées Y des previsions aladin.
        t (list): Temperature moyenne journalière des previsions aladin.
        

    Returns:
        X(list): Coordonnées X .
        Y(list): Coordonnées Y .
        nb_j(list): Nombre de jour de gel annuel pour chaque point
    """
    X = []
    Y = []
    nb_j = []
    c = 0
    i =0
    j=0

    while i < len(t):
       X.append(x[i])
       Y.append(y[i])
       while x[i] == X[j] and y[i] == Y[j] : 
           if t[i] > 30 :
               c+=1
           i+=1
           if i >= len(t):
               break
       nb_j.append(c)
       c=0
       j+=1
    return X,Y,nb_j

def vagues_froids (x,y,T):
    """
    Calculer le nombre et la durée cumulé des vague de froides pour chaque point.

    Args:
        x (list: Coordonnées X des previsions aladin.
        y (list): Coordonnées Y des previsions aladin.
        T (list): Temperature minimal journalière des previsions aladin.
        

    Returns:
        X(list): Coordonnées X .
        Y(list): Coordonnées Y .
        d_vagues(list):Durée devague de froids cumuluée pour chaque point.
        nb_vagues(list): Nombre total annuelle de vague de froid pour chaque point.
    """
    X = []
    Y = []
    i = 1
    d = 0
    j=0
    t = []
    d_vagues = []
    nb_vagues =[]
    v =0
    while i < len(T):
        X.append(x[i])
        Y.append(y[i])
        d_vagues.append(0)
        nb_vagues.append(0)
        while x[i] == X[j] and y[i] == Y[j] :
           
                if T[i] < 0.9 and T[i-1] < 0.9 and T[i+1]< 0.9:
                    t.append(T[i])
                    d+=1
                    
                if T[i] > 2.2 :
                   if  len(t) != 0 :
                        if  min(t) < -2 :
                               v += 1
                               nb_vagues[j] += v
                               d_vagues[j] += d
                   t = []
                   d =0
                   v =0
                   i+=1
                    
                i+=1
                if i >= (len(T)-1):
                    break
                
                
        if  len(t) != 0 :
            if min(t) < -2 :
                v += 1
                nb_vagues[j] += v
                d_vagues[j] += d
        v =0        
        d =0
        i+=1
        t = []
        j+=1
    
    
    return X,Y, d_vagues, nb_vagues

def tot_p_solide(x,y,p):
    """
    Calculer le total cumulé des précipitation solide annuelle pour chaque point.

    Args:
        x (list: Coordonnées X des previsions aladin.
        y (list): Coordonnées Y des previsions aladin.
        p (list): Precipitation solide des previsions aladin.
        

    Returns:
        X(list): Coordonnées X .
        Y(list): Coordonnées Y .
        tp(list):total cumulé des précipitation solide annuelle pour chaque point.
        
    """
    X = []
    Y = []
    tp = []
    c = 0
    i =0
    j=0

    while i < len(p):
       X.append(x[i])
       Y.append(y[i])
       while x[i] == X[j] and y[i] == Y[j] : 
           c+= p[i]
           i +=1
           if i >= len(p):
               break
       tp.append(c)
       c=0
       j+=1
    return X,Y,tp
            
def max_p_solide(x,y,p):
    """
    Calculer le maximum local des précipitation solide annuelle pour chaque point.

    Args:
        x (list: Coordonnées X des previsions aladin.
        y (list): Coordonnées Y des previsions aladin.
        p (list): Precipitation solide des previsions aladin.
        

    Returns:
        X(list): Coordonnées X .
        Y(list): Coordonnées Y .
        maxp(list):maximum local des précipitation solide annuelle pour chaque point.
        
    """
    X = []
    Y = []
    maxp = []
    i =0
    j=0
    c=p[0]

    while i < len(p):
       X.append(x[i])
       Y.append(y[i])
       while x[i] == X[j] and y[i] == Y[j] : 

           if c < p[i]:
               c = p[i]

           i+=1
           if i == len(p):
               break

           
       maxp.append(c)
       c=p[0]
       j+=1
    return X,Y,maxp