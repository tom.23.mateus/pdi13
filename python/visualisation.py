# -*- coding: utf-8 -*-
"""
Created on Tue May  7 14:09:09 2024

@author: Tom
"""
import pandas as pd
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt


def interp_inv_dist(x_grd,y_grd,x_obs,y_obs,z_obs,p):
     """
     Calcul d'une interpolation par inverse de distance

     Args:
         x_grd (List(List)): Coordonnées X des points de la grilles.
         y_grd (List(List)): Coordonnées X des points de la grilles.
         x_obs (List): Coordonnées X des observations.
         y_obs(List): Coordonnées Y des observations
         z_obs(List): Valeur de l'indicateru a interpoller des observations
         p (float): Pondération
             

     Returns:
         z(List(List)): indicateur interpolé.
     """
     z = np.nan*np.zeros(x_grd.shape)
     d=[]
     i =0
     j = 0

     for i in np.arange(0,x_grd.shape[0]): 
          for j in np.arange(0,x_grd.shape[1]):
             
               d = np.sqrt((x_grd[i,j]-x_obs)**2+(y_grd[i,j]-y_obs)**2)
               n =np.sum(z_obs/d**p)
               dn = np.sum(1/d**p)
               z[i,j] = n/dn 
     return z
 
    

def save_shp(x,y,v,name,limite):
    """
     Sauvegarder les donnée en shapefile

     Args:
         x(List(List)): Coordonnées X des points de la grilles.
         y (List(List)): Coordonnées X des points de la grilles.
         z_obs(List(List)): Valeur de l'indicateur interpoller 
         name (str): nom du fichier de sortie type 'nombre_jour_gel_2030_rcp4.5'
         limite(str): adrresse du fichier shapephile de la limite de la zone a cartographier
             

     Returns:
         z(List(List)): indicateur interpolé.
    """
    
    
    
    X = x[0]
    Y = y[0]
    V = v[0]
    for i in range (np.shape(x)[0]-1):
            X = np.concatenate((X,x[i+1] ), axis=None)
            Y = np.concatenate((Y,y[i+1] ), axis=None)
            V = np.concatenate((V,v[i+1] ), axis=None)
            
    print(X,Y,V)


            
    
    df = pd.DataFrame({'X': X, 'Y': Y, name:V})
    gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.X, df.Y), crs="EPSG:4326")

    
    # Charger le fichier de points et de limite

    limite_gdf = gpd.read_file(limite)
    
    limite_gdf = limite_gdf.to_crs('EPSG:4326')

    
    # Unifier la limite en un seul polygone
    geometrie_limite_unifiee = limite_gdf.unary_union
    
    
    # Effectuer une opération d'intersection entre les points et la limite
    points_extraits_gdf = gdf[gdf.geometry.within(geometrie_limite_unifiee)]
    # sauvetage du fichier shapefile
    points_extraits_gdf.to_file('sortie_shp/'+ name +'.shp')
    print( points_extraits_gdf.head())
    
def plot_patch(x_obs, y_obs, z_obs, xlabel = "", ylabel = "", zlabel = "", title = "", fileo = "", cmap = "", minmax =""):
    """
     Afficher les cartes

     Args:
         x_obs (List): Coordonnées X des observations.
         y_obs(List): Coordonnées Y des observations
         z_obs(List): Valeur de l'indicateru a interpoller des observations
         xlabel, ylabel, zlabel (str) : étiquettes des axes (facultatif)
         title (str): titre (facultatif)
         fileo(str) : adresse du fichier d'enregistrement de la figure (facultatif)
         cmap (str) : nom de la carte de couleur
         minmax (List): list de 2 élément , le minimum et le maximum (int ou float)
         
    """
    
    # Tracé des valeurs observées
    # x_obs, y_obs, z_obs : observations
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    # cmap : nom de la carte de couleur
    
    fig = plt.figure()
    p=plt.scatter(x_obs, y_obs, marker = 'o', c = z_obs, s = 80, cmap=cmap, vmin = minmax[0], vmax = minmax[-1])

    dx = max(x_obs)-min(x_obs)
    dy = max(y_obs)-min(y_obs)
    minx = min(x_obs)-0.05*dx; maxx = max(x_obs)+0.05*dx
    miny = min(y_obs)-0.05*dy; maxy = max(y_obs)+0.05*dy
    plt.xlim([minx,maxx])
    plt.ylim([miny,maxy])
    #plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')
    plt.gca()

