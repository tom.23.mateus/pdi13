import geopandas as gpd
import os


def extraire_points_par_limite(points, limite, sortie):
    """
    Extrait tous les points contenus dans une limite.

    Args:
        points (str): Chemin vers le fichier de points (shapefile).
        limite (str): Chemin vers le fichier de limite (shapefile).

    Returns:
        GeoDataFrame: Un GeoDataFrame contenant les points extraits.
    """
    # Charger le fichier de points et de limite
    points_gdf = gpd.read_file(points)
    limite_gdf = gpd.read_file(limite)
    
    points_gdf = points_gdf.to_crs('EPSG:4326')
    limite_gdf = limite_gdf.to_crs('EPSG:4326')

    
    # Unifier la limite en un seul polygone
    geometrie_limite_unifiee = limite_gdf.unary_union
    # Effectuer une opération d'intersection entre les points et la limite
    points_extraits_gdf = points_gdf[points_gdf.geometry.within(geometrie_limite_unifiee)]
    points_extraits_gdf.to_file(sortie)
    
    print("Points extraits enregistrés dans:", sortie)
    
    return points_extraits_gdf,points_gdf,limite_gdf




    # Exemple d'utilisation    
if __name__ == "__main__":

    fichier_points = 'donnees/temp.shp'
    fichier_limite = 'donnees/Gironde.shp'
    repertoire_sortie  = 'donnees/'  
    nom_fichier_sortie = 'TempGironde.shp' 
    
    # Concaténer le répertoire de sortie avec le nom de fichier de sortie
    chemin_sortie = os.path.join(repertoire_sortie, nom_fichier_sortie)
    
    # Appel de la fonction extraire_points_par_limite avec les chemins corrects
    points_extraits_gdf, points_gdf,limite_gdf  = extraire_points_par_limite(fichier_points, fichier_limite, chemin_sortie)
    
    # Afficher les points extraits
    print("Points extraits:")
    print(points_extraits_gdf.head())

