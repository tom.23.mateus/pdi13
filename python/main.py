# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 09:09:36 2024

@author: Tom
"""
import numpy as np
import geopandas as gpd
import time


print("                                                 ")
time.sleep(0.1)  
print("                                                 ")
time.sleep(0.1)  
print("                                                 ")
time.sleep(0.1)  
print("  /$$$$$$                      /$$       /$$             /$$               /$$    /$$          /$$                            /$$$$$$            /$$                     /$$             /$$                        ")    
time.sleep(0.1)  
print(" /$$__  $$                    |__/      | $$            | $$              | $$   | $$         | $$                           /$$__  $$          | $$                    | $$            | $$                        ")
time.sleep(0.1)  
print("| $$  \ $$  /$$$$$$   /$$$$$$  /$$  /$$$$$$$  /$$$$$$  /$$$$$$    /$$$$$$ | $$   | $$ /$$$$$$ | $$ /$$   /$$  /$$$$$$       | $$  \__/  /$$$$$$ | $$  /$$$$$$$ /$$   /$$| $$  /$$$$$$  /$$$$$$    /$$$$$$   /$$$$$$ ")
time.sleep(0.1)  
print("| $$$$$$$$ /$$__  $$ /$$__  $$| $$ /$$__  $$ |____  $$|_  $$_/   |____  $$|  $$ / $$/|____  $$| $$| $$  | $$ /$$__  $$      | $$       |____  $$| $$ /$$_____/| $$  | $$| $$ |____  $$|_  $$_/   /$$__  $$ /$$__  $$")
time.sleep(0.1)  
print("| $$__  $$| $$  \ $$| $$  \__/| $$| $$  | $$  /$$$$$$$  | $$      /$$$$$$$ \  $$ $$/  /$$$$$$$| $$| $$  | $$| $$$$$$$$      | $$        /$$$$$$$| $$| $$      | $$  | $$| $$  /$$$$$$$  | $$    | $$  \ $$| $$  \__/")
time.sleep(0.1)  
print("| $$  | $$| $$  | $$| $$      | $$| $$  | $$ /$$__  $$  | $$ /$$ /$$__  $$  \  $$$/  /$$__  $$| $$| $$  | $$| $$_____/      | $$    $$ /$$__  $$| $$| $$      | $$  | $$| $$ /$$__  $$  | $$ /$$| $$  | $$| $$      ")
time.sleep(0.1)  
print("| $$  | $$|  $$$$$$$| $$      | $$|  $$$$$$$|  $$$$$$$  |  $$$$/|  $$$$$$$   \  $/  |  $$$$$$$| $$|  $$$$$$/|  $$$$$$$      |  $$$$$$/|  $$$$$$$| $$|  $$$$$$$|  $$$$$$/| $$|  $$$$$$$  |  $$$$/|  $$$$$$/| $$      ")
time.sleep(0.1) 
print("|__/  |__/ \____  $$|__/      |__/ \_______/ \_______/   \___/   \_______/    \_/    \_______/|__/ \______/  \_______//$$$$$$\______/  \_______/|__/ \_______/ \______/ |__/ \_______/   \___/   \______/ |__/     ")
time.sleep(0.1) 
print("           /$$  \ $$                                                                                                 |______/                                                                                       ")
time.sleep(0.1)
print("          |  $$$$$$/                                                                                                                                                                                                ")
time.sleep(0.1) 
print("           \______/                                                                                                                                                                                                 ")
time.sleep(0.1)  
print("                                                 ")
time.sleep(0.1)  
print("                                                 ")
time.sleep(0.1)  
print("                                                 ")
time.sleep(0.1)  

from indicateur import tot_p_solide , max_p_solide,vagues_froids,nb_j_gel,nb_j_chaud
import visualisation as vis
from classif import classification



def extraction_lat_long_v (points):
    """
    extrait les donée du shapefile

    Args:
        points (str): addresse du shapefiles d'entrée (Données Aladin au format shapefile).

        

    Returns:
        x(list): Coordonnées X des points .
        y(list): Coordonnées Y des points .
        d(list): Temperature minimal journalière pour chaque points .
        t(list): Temperature moyenne journalière pour chaque points .
        p(list): Précipitation solide journalière pour chaque point
    """
    
    x = []
    y = []
    d = []
    t = []
    p = []
    
    # Charger le fichier de points et de limite
    points_gdf = gpd.read_file(points)
    
    points_gdf = points_gdf.to_crs('EPSG:4326')




    for i in range (len(points_gdf)):
        
            y.append(float(points_gdf['0'][i]))
            x.append(float(points_gdf['1'][i])) 
            d.append(float(points_gdf['5'][i]))
            t.append(float(points_gdf['6'][i]))
            p.append(float(points_gdf['7'][i]))
            
    return x,y,d,t,p
            


 


if __name__ == "__main__":
    
        r = 1000
    

        while True :
            a = (input("Choix du rcp :4.5(1) ou 8.5(2) ?"))
            if a == '1' or a == '4.5' :
                rcp = 4.5 
                break
            if a == '2' or a == '8.5' :
                rcp = 8.5
                break
        while True :
            a = (input("Visualistion des cartes des indicateurs agroclimatique ? (yes/no)"))
            if a == 'yes' or a=='y' :
                v = True
                break
            if a == 'no'or a=='n' : 
                v = False
                break
            
  
            
        if rcp == 4.5 :
            print('ouverture des données')
            x30,y30,tm30,t30,p30 = extraction_lat_long_v('donnees/temps_2030_4.5.shp')
            x50,y50,tm50,t50,p50 = extraction_lat_long_v('donnees/temps_2050_4.5.shp')
            x70,y70,tm70,t70,p70 = extraction_lat_long_v('donnees/temps_2070_4.5.shp')
            x100,y100,tm100,t100,p100 = extraction_lat_long_v('donnees/temps_2100_4.5.shp')
            print('données ouvertes')
            
           
            #calcul du nombre de jour chaud
            print('calcul nombre de jour chaud...')
            X30c,Y30c,nb_j_c30 = nb_j_chaud(x30,y30,t30)
            X50c,Y50c,nb_j_c50 = nb_j_chaud(x50,y50,t50)
            X70c,Y70c,nb_j_c70 = nb_j_chaud(x70,y70,t70)
            X100c,Y100c,nb_j_c100 = nb_j_chaud(x100,y100,t100)
            print('fin de calcul')
            # Création d'une grille planimétrique pour l'interpolation
            x_grd, y_grd = np.meshgrid(np.linspace(np.floor(np.min(X30c)), np.ceil(np.max(X30c)), r), np.linspace(np.floor(np.min(Y30c)), np.ceil(np.max(Y30c)), r))
            
            
             
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(y_grd,x_grd,Y30c,X30c,nb_j_c30,2)
            z_grd_lin_50 = vis.interp_inv_dist(y_grd,x_grd,Y50c,X50c,nb_j_c50,2)
            z_grd_lin_70 = vis.interp_inv_dist(y_grd,x_grd,Y70c,X70c,nb_j_c70,2)
            z_grd_lin_100 = vis.interp_inv_dist(y_grd,x_grd,Y100c,X100c,nb_j_c100,2)
            print('Interpolation terminer')
        
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'nombre_jour_chaud_2030_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'nombre_jour_chaud_2050_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'nombre_jour_chaud_2070_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'nombre_jour_chaud_2100_rcp4.5','donnees/SaintEmilion.shp')

            #calcul du nombre de jour de gel
            print('calcul nombre de jour de gel')
            X30,Y30,nb_j_g30 = nb_j_gel(x30,y30,tm30)
            X50,Y50,nb_j_g50 = nb_j_gel(x50,y50,tm50)
            X70,Y70,nb_j_g70 = nb_j_gel(x70,y70,tm70)
            X100,Y100,nb_j_g100 = nb_j_gel(x100,y100,tm100)
            print('fin de calcul')
            
             
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(x_grd,y_grd,Y30,X30,nb_j_g30,2)
            z_grd_lin_50 = vis.interp_inv_dist(x_grd,y_grd,Y50,X50,nb_j_g50,2)
            z_grd_lin_70 = vis.interp_inv_dist(x_grd,y_grd,Y70,X70,nb_j_g70,2)
            z_grd_lin_100 = vis.interp_inv_dist(x_grd,y_grd,Y100,X100,nb_j_g100,2)
            print('Interpolation terminer')
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'nombre_jour_gel_2030_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'nombre_jour_gel_2050_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'nombre_jour_gel_2070_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'nombre_jour_gel_2100_rcp4.5','donnees/SaintEmilion.shp')


            #calcul des vagues de froids
            print('calcul nombre et durée de vague de froid')
            X30v,Y30v, d_vagues30, nb_vagues30 = vagues_froids(x30,y30,tm30)
            X50v,Y50v, d_vagues50, nb_vagues50 = vagues_froids(x50,y50,tm50)
            X70v,Y70v, d_vagues70, nb_vagues70 = vagues_froids(x70,y70,tm70)
            X100v,Y100v, d_vagues100, nb_vagues100 = vagues_froids(x100,y100,tm100)
            print('fin de calcul')
            
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(x_grd,y_grd,Y30v,X30v,nb_vagues30,2)
            z_grd_lin_50 = vis.interp_inv_dist(x_grd,y_grd,Y50v,X50v,nb_vagues50,2)
            z_grd_lin_70 = vis.interp_inv_dist(x_grd,y_grd,Y70v,X70v,nb_vagues70,2)
            z_grd_lin_100 = vis.interp_inv_dist(x_grd,y_grd,Y100v,X100v,nb_vagues100,2)
            print('Interpolation terminer')
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'nombre_de_vague_de_froid_2030_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'nombre_de_vague_de_froid_2050_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'nombre_de_vague_de_froid_2070_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'nombre_de_vague_de_froid_2100_rcp4.5','donnees/SaintEmilion.shp')
     
            # calcule maximum de precipitation solide 
            print('calcul maximum journalier de precipitation solide...')
            X30mp,Y30mp,mp30 = max_p_solide(x30,y30,p30)
            X50mp,Y50mp,mp50 = max_p_solide(x50,y50,p50)
            X70mp,Y70mp,mp70 = max_p_solide(x70,y70,p70)
            X100mp,Y100mp,mp100 = max_p_solide(x100,y100,p100)
            print('fin de calcul')
            
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(x_grd,y_grd,Y30mp,X30mp,mp30,2)
            z_grd_lin_50 = vis.interp_inv_dist(x_grd,y_grd,Y50mp,X50mp,mp50,2)
            z_grd_lin_70 = vis.interp_inv_dist(x_grd,y_grd,Y70mp,X70mp,mp70,2)
            z_grd_lin_100 = vis.interp_inv_dist(x_grd,y_grd,Y100mp,X100mp,mp100,2)
            print('Interpolation terminer')
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'maximum_precipitation_solide_journalière_2030_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'maximum_precipitation_solide_journalière_2050_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'maximum_precipitation_solide_journalière_2070_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'maximum_precipitation_solide_journalièred_2100_rcp4.5','donnees/SaintEmilion.shp')
     
           # calcul total precipitaton solide
            print('calcul du total de precipitation solide...')
            X30tp , Y30tp, tp30 = tot_p_solide(x30,y30,p30)
            X50tp,Y50tp,tp50 = tot_p_solide(x50,y50,p50)
            X70tp,Y70tp,tp70 = tot_p_solide(x70,y70,p70)
            X100tp,Y100tp,tp100 = tot_p_solide(x100,y100,p100) 
            print('fin de calcul')
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(x_grd,y_grd,Y30tp,X30tp,tp30,2)
            z_grd_lin_50 = vis.interp_inv_dist(x_grd,y_grd,Y50tp,X50tp,tp50,2)
            z_grd_lin_70 = vis.interp_inv_dist(x_grd,y_grd,Y70tp,X70tp,tp70,2)
            z_grd_lin_100 = vis.interp_inv_dist(x_grd,y_grd,Y100tp,X100tp,tp100,2)
            print('Interpolation terminer')
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'total_precipitation_solide_2030_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'total_precipitation_solide_2050_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'total_precipitation_solide_2070_rcp4.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'total_precipitation_solide_2100_rcp4.5','donnees/SaintEmilion.shp')
     
           

            if v :
              #Visualisation nobre de jour de chaud
                nb_min_t = min(np.min(nb_j_c30),np.min(nb_j_c50),np.min(nb_j_c70),np.min(nb_j_c100))
                nb_max_t = max(np.max(nb_j_c30),np.max(nb_j_c50),np.max(nb_j_c70),np.max(nb_j_c100))
             
                vis.plot_patch(X30c, Y30c, nb_j_c30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de chaleur en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X50c, Y50c, nb_j_c50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de chaleur en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X70c, Y70c, nb_j_c70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de chaleur en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X100c, Y100c, nb_j_c100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de chaleur en 2100rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
            
              # Visualisation nombre de jour de gel
                nb_min_t = min(np.min(nb_j_g30),np.min(nb_j_g50),np.min(nb_j_g70),np.min(nb_j_g100))
                nb_max_t = max(np.max(nb_j_g30),np.max(nb_j_g50),np.max(nb_j_g70),np.max(nb_j_g100))
              
                vis.plot_patch(X30, Y30, nb_j_g30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de gel en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X50, Y50, nb_j_g50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de gel en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X70, Y70, nb_j_g70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de gel en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X100, Y100, nb_j_g100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de gel en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
              
                # Visualisation durée des vagues
              
                nb_min_dv = min(np.min(d_vagues30),np.min(d_vagues50),np.min(d_vagues70),np.min(d_vagues100))
                nb_max_dv = max(np.max(d_vagues30),np.max(d_vagues50),np.max(d_vagues70),np.max(d_vagues100))
              

                vis.plot_patch(X30v, Y30v, d_vagues30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "durée de vague de froid en jour en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_dv, nb_max_dv])
                vis.plot_patch(X50v, Y50v, d_vagues50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "durée de vague de froid en jour en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_dv, nb_max_dv])
                vis.plot_patch(X70v, Y70v, d_vagues70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "durée de vague de froid en jour en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_dv, nb_max_dv])
                vis.plot_patch(X100v, Y100v, d_vagues100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "durée de vague de froid en jour en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_dv, nb_max_dv])
              
              # Visualisation nb des vagues
              
                nb_min_nbv = min(np.min(nb_vagues30),np.min(nb_vagues50),np.min(nb_vagues70),np.min(nb_vagues100))
                nb_max_nbv = max(np.max(nb_vagues30),np.max(nb_vagues50),np.max(nb_vagues70),np.max(nb_vagues100))
                
                vis.plot_patch(X30v, Y30v, nb_vagues30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de vague de froid en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_nbv, nb_max_nbv])
                vis.plot_patch(X50v, Y50v,  nb_vagues50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de vague de froid en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_nbv, nb_max_nbv])
                vis.plot_patch(X70v, Y70v,  nb_vagues70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de vaguede froid  en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_nbv, nb_max_nbv])
                vis.plot_patch(X100v, Y100v, nb_vagues100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de vague de froid en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_nbv, nb_max_nbv])
              
                # Visualisation du maximum journalier des precipitation solide
                
                nb_min_mp = min(np.min(mp30),np.min(mp50),np.min(mp70),np.min(mp100))
                nb_max_mp = max(np.max(mp30),np.max(mp50),np.max(mp70),np.max(mp100))
                
                vis.plot_patch(X30mp, Y30mp, mp30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "maximum de precipitation solide journalière en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_mp, nb_max_mp])
                vis.plot_patch(X50mp, Y50mp,  mp50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "maximum de precipitation solide journalière en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_mp, nb_max_mp])
                vis.plot_patch(X70mp, Y70mp,  mp70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "maximum de precipitation solide journalière en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_mp, nb_max_mp])
                vis.plot_patch(X100mp, Y100mp, mp100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "maximum de precipitation solide journalière en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_mp, nb_max_mp])
             
                # Visualisation du total des precipitation solide
                
                nb_min_tp = min(np.min(tp30),np.min(tp50),np.min(tp70),np.min(tp100))
                nb_max_tp = max(np.max(tp30),np.max(tp50),np.max(tp70),np.max(tp100))
                
                vis.plot_patch(X30tp , Y30tp, tp30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "total de precipitation solide en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_tp, nb_max_tp])
                vis.plot_patch(X50tp,Y50tp,tp50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "total de precipitation solide en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [  nb_min_tp, nb_max_tp])
                vis.plot_patch(X70tp,Y70tp,tp70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "total de precipitation solide en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [  nb_min_tp, nb_max_tp])
                vis.plot_patch(X100tp,Y100tp,tp100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "total de precipitation solide journalière en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [  nb_min_tp, nb_max_tp])
              

        else :
            print('ouverture des données')
            x30,y30,tm30,t30,p30 = extraction_lat_long_v('donnees/temps_2030_8.5.shp')
            x50,y50,tm50,t50,p50 = extraction_lat_long_v('donnees/temps_2050_8.5.shp')
            x70,y70,tm70,t70,p70 = extraction_lat_long_v('donnees/temps_2070_8.5.shp')
            x100,y100,tm100,t100,p100 = extraction_lat_long_v('donnees/temps_2100_8.5.shp')
            print('données ouvertes')
            
          
            #calcul du nombre de jour chaud
            print('calcul nombre de jour chaud...')
            X30c,Y30c,nb_j_c30 = nb_j_chaud(x30,y30,t30)
            X50c,Y50c,nb_j_c50 = nb_j_chaud(x50,y50,t50)
            X70c,Y70c,nb_j_c70 = nb_j_chaud(x70,y70,t70)
            X100c,Y100c,nb_j_c100 = nb_j_chaud(x100,y100,t100)
            print('fin de calcul')
            
            # Création d'une grille planimétrique pour l'interpolation
            x_grd, y_grd = np.meshgrid(np.linspace(np.floor(np.min(X30c)), np.ceil(np.max(X30c)), r), np.linspace(np.floor(np.min(Y30c)), np.ceil(np.max(Y30c)), r))
           
           
            
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(y_grd,x_grd,Y30c,X30c,nb_j_c30,2)
            z_grd_lin_50 = vis.interp_inv_dist(y_grd,x_grd,Y50c,X50c,nb_j_c50,2)
            z_grd_lin_70 = vis.interp_inv_dist(y_grd,x_grd,Y70c,X70c,nb_j_c70,2)
            z_grd_lin_100 = vis.interp_inv_dist(y_grd,x_grd,Y100c,X100c,nb_j_c100,2)
            print('Interpolation terminer')
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'nombre_jour_chaud_2030_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'nombre_jour_chaud_2050_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'nombre_jour_chaud_2070_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'nombre_jour_chaud_2100_rcp8.5','donnees/SaintEmilion.shp') 

            #calcul du nombre de jour de gel
            print('calcul nombre de jour de gel')
            X30,Y30,nb_j_g30 = nb_j_gel(x30,y30,tm30)
            X50,Y50,nb_j_g50 = nb_j_gel(x50,y50,tm50)
            X70,Y70,nb_j_g70 = nb_j_gel(x70,y70,tm70)
            X100,Y100,nb_j_g100 = nb_j_gel(x100,y100,tm100)
            print('fin de calcul')
           
            
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(y_grd,x_grd,Y30,X30,nb_j_g30,2)
            z_grd_lin_50 = vis.interp_inv_dist(y_grd,x_grd,Y50,X50,nb_j_g50,2)
            z_grd_lin_70 = vis.interp_inv_dist(y_grd,x_grd,Y70,X70,nb_j_g70,2)
            z_grd_lin_100 = vis.interp_inv_dist(y_grd,x_grd,Y100,X100,nb_j_g100,2)
            print('Interpolation terminer')
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'nombre_jour_gel_2030_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'nombre_jour_gel_2050_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'nombre_jour_gel_2070_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'nombre_jour_gel_2100_rcp8.5','donnees/SaintEmilion.shp')


            #calcul des vagues de froids
            print('calcul nombre et durée de vague de froid')
            X30v,Y30v, d_vagues30, nb_vagues30 = vagues_froids(x30,y30,tm30)
            X50v,Y50v, d_vagues50, nb_vagues50 = vagues_froids(x50,y50,tm50)
            X70v,Y70v, d_vagues70, nb_vagues70 = vagues_froids(x70,y70,tm70)
            X100v,Y100v, d_vagues100, nb_vagues100 = vagues_froids(x100,y100,tm100)
            print('fin de calcul')
           
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(y_grd,x_grd,Y30v,X30v,nb_vagues30,2)
            z_grd_lin_50 = vis.interp_inv_dist(y_grd,x_grd,Y50v,X50v,nb_vagues50,2)
            z_grd_lin_70 = vis.interp_inv_dist(y_grd,x_grd,Y70v,X70v,nb_vagues70,2)
            z_grd_lin_100 = vis.interp_inv_dist(y_grd,x_grd,Y100v,X100v,nb_vagues100,2)
            print('Interpolation terminer')
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'nombre_de_vague_de_froid_2030_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'nombre_de_vague_de_froid_2050_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'nombre_de_vague_de_froid_2070_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'nombre_de_vague_de_froid_2100_rcp8.5','donnees/SaintEmilion.shp')
    
            # calcule maximum de precipitation solide 
            print('calcul maximum journalier de precipitation solide...')
            X30mp,Y30mp,mp30 = max_p_solide(x30,y30,p30)
            X50mp,Y50mp,mp50 = max_p_solide(x50,y50,p50)
            X70mp,Y70mp,mp70 = max_p_solide(x70,y70,p70)
            X100mp,Y100mp,mp100 = max_p_solide(x100,y100,p100)
            print('fin de calcul')
           
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(y_grd,x_grd,Y30mp,X30mp,mp30,2)
            z_grd_lin_50 = vis.interp_inv_dist(y_grd,x_grd,Y50mp,X50mp,mp50,2)
            z_grd_lin_70 = vis.interp_inv_dist(y_grd,x_grd,Y70mp,X70mp,mp70,2)
            z_grd_lin_100 = vis.interp_inv_dist(y_grd,x_grd,Y100mp,X100mp,mp100,2)
            print('Interpolation terminer')
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'maximum_precipitation_solide_journalière_2030_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'maximum_precipitation_solide_journalière_2050_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'maximum_precipitation_solide_journalière_2070_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'maximum_precipitation_solide_journalièred_2100_rcp8.5','donnees/SaintEmilion.shp')
    
           # calcul total precipitaton solide
            print('calcul du total de precipitation solide...')
            X30tp , Y30tp, tp30 = tot_p_solide(x30,y30,p30)
            X50tp,Y50tp,tp50 = tot_p_solide(x50,y50,p50)
            X70tp,Y70tp,tp70 = tot_p_solide(x70,y70,p70)
            X100tp,Y100tp,tp100 = tot_p_solide(x100,y100,p100) 
            print('fin de calcul')
            print('Début de linterpolation')
            z_grd_lin_30 = vis.interp_inv_dist(y_grd,x_grd,Y30tp,X30tp,tp30,2)
            z_grd_lin_50 = vis.interp_inv_dist(y_grd,x_grd,Y50tp,X50tp,tp50,2)
            z_grd_lin_70 = vis.interp_inv_dist(y_grd,x_grd,Y70tp,X70tp,tp70,2)
            z_grd_lin_100 = vis.interp_inv_dist(y_grd,x_grd,Y100tp,X100tp,tp100,2)
            print('Interpolation terminer')
            #sauvegarde en shp
            vis.save_shp(x_grd,y_grd,  z_grd_lin_30,'total_precipitation_solide_2030_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_50,'total_precipitation_solide_2050_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_70,'total_precipitation_solide_2070_rcp8.5','donnees/SaintEmilion.shp')
            vis.save_shp(x_grd,y_grd,  z_grd_lin_100,'total_precipitation_solide_2100_rcp8.5','donnees/SaintEmilion.shp')
            
            if v :
              #Visualisation nobre de jour de chaud
                nb_min_t = min(np.min(nb_j_c30),np.min(nb_j_c50),np.min(nb_j_c70),np.min(nb_j_c100))
                nb_max_t = max(np.max(nb_j_c30),np.max(nb_j_c50),np.max(nb_j_c70),np.max(nb_j_c100))
             
                vis.plot_patch(X30c, Y30c, nb_j_c30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de chaleur en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X50c, Y50c, nb_j_c50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de chaleur en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X70c, Y70c, nb_j_c70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de chaleur en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X100c, Y100c, nb_j_c100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de chaleur en 2100rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
            
              # Visualisation nombre de jour de gel
                nb_min_t = min(np.min(nb_j_g30),np.min(nb_j_g50),np.min(nb_j_g70),np.min(nb_j_g100))
                nb_max_t = max(np.max(nb_j_g30),np.max(nb_j_g50),np.max(nb_j_g70),np.max(nb_j_g100))
              
                vis.plot_patch(X30, Y30, nb_j_g30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de gel en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X50, Y50, nb_j_g50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de gel en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X70, Y70, nb_j_g70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de gel en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
                vis.plot_patch(X100, Y100, nb_j_g100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de jours de gel en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_t, nb_max_t])
              
                # Visualisation durée des vagues
              
                nb_min_dv = min(np.min(d_vagues30),np.min(d_vagues50),np.min(d_vagues70),np.min(d_vagues100))
                nb_max_dv = max(np.max(d_vagues30),np.max(d_vagues50),np.max(d_vagues70),np.max(d_vagues100))
              

                vis.plot_patch(X30v, Y30v, d_vagues30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "durée de vague de froid en jour en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_dv, nb_max_dv])
                vis.plot_patch(X50v, Y50v, d_vagues50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "durée de vague de froid en jour en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_dv, nb_max_dv])
                vis.plot_patch(X70v, Y70v, d_vagues70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "durée de vague de froid en jour en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_dv, nb_max_dv])
                vis.plot_patch(X100v, Y100v, d_vagues100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "durée de vague de froid en jour en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_dv, nb_max_dv])
              
              # Visualisation nb des vagues
              
                nb_min_nbv = min(np.min(nb_vagues30),np.min(nb_vagues50),np.min(nb_vagues70),np.min(nb_vagues100))
                nb_max_nbv = max(np.max(nb_vagues30),np.max(nb_vagues50),np.max(nb_vagues70),np.max(nb_vagues100))
                
                vis.plot_patch(X30v, Y30v, nb_vagues30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de vague de froid en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_nbv, nb_max_nbv])
                vis.plot_patch(X50v, Y50v,  nb_vagues50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de vague de froid en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_nbv, nb_max_nbv])
                vis.plot_patch(X70v, Y70v,  nb_vagues70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de vaguede froid  en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_nbv, nb_max_nbv])
                vis.plot_patch(X100v, Y100v, nb_vagues100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "nombre de vague de froid en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_nbv, nb_max_nbv])
              
                # Visualisation du maximum journalier des precipitation solide
                
                nb_min_mp = min(np.min(mp30),np.min(mp50),np.min(mp70),np.min(mp100))
                nb_max_mp = max(np.max(mp30),np.max(mp50),np.max(mp70),np.max(mp100))
                
                vis.plot_patch(X30mp, Y30mp, mp30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "maximum de precipitation solide journalière en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_mp, nb_max_mp])
                vis.plot_patch(X50mp, Y50mp,  mp50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "maximum de precipitation solide journalière en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_mp, nb_max_mp])
                vis.plot_patch(X70mp, Y70mp,  mp70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "maximum de precipitation solide journalière en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_mp, nb_max_mp])
                vis.plot_patch(X100mp, Y100mp, mp100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "maximum de precipitation solide journalière en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_mp, nb_max_mp])
             
                # Visualisation du total des precipitation solide
                
                nb_min_tp = min(np.min(tp30),np.min(tp50),np.min(tp70),np.min(tp100))
                nb_max_tp = max(np.max(tp30),np.max(tp50),np.max(tp70),np.max(tp100))
                
                vis.plot_patch(X30tp , Y30tp, tp30, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "total de precipitation solide en 2030 rcp "+ str(rcp) , cmap = 'inferno',minmax = [ nb_min_tp, nb_max_tp])
                vis.plot_patch(X50tp,Y50tp,tp50, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "total de precipitation solide en 2050 rcp "+ str(rcp) , cmap = 'inferno',minmax = [  nb_min_tp, nb_max_tp])
                vis.plot_patch(X70tp,Y70tp,tp70, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "total de precipitation solide en 2070 rcp "+ str(rcp) , cmap = 'inferno',minmax = [  nb_min_tp, nb_max_tp])
                vis.plot_patch(X100tp,Y100tp,tp100, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "total de precipitation solide journalière en 2100 rcp "+ str(rcp) , cmap = 'inferno',minmax = [  nb_min_tp, nb_max_tp])
              
        #Calcule de Vulnérabilité et classification
        print('calcul de la Vulnérabilité')
        classification('nombre_jour_chaud_2030_rcp'+str(rcp), 'nombre_jour_gel_2030_rcp'+str(rcp), 'total_precipitation_solide_2030_rcp'+str(rcp), rcp, '2030')
        classification('nombre_jour_chaud_2050_rcp'+str(rcp), 'nombre_jour_gel_2050_rcp'+str(rcp), 'total_precipitation_solide_2050_rcp'+str(rcp), rcp, '2050')
        classification('nombre_jour_chaud_2070_rcp'+str(rcp), 'nombre_jour_gel_2070_rcp'+str(rcp), 'total_precipitation_solide_2070_rcp'+str(rcp), rcp, '2070')
        classification('nombre_jour_chaud_2100_rcp'+str(rcp), 'nombre_jour_gel_2100_rcp'+str(rcp), 'total_precipitation_solide_2100_rcp'+str(rcp), rcp, '2100')
        
        

        
          
    
      
        
      
        
  